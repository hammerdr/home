FROM nginx:latest

COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY ./homeapp/build /usr/share/nginx/html
COPY ./obsidian/output/html /usr/share/nginx/wiki
COPY ./archive/public_html /usr/share/nginx/archive
COPY ./website /usr/share/nginx/www
