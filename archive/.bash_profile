# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin
PATH=$PATH:$HOME/ruby/gems/bin
export GEM_PATH=$HOME/ruby/gems:/usr/lib/ruby/gems/1.8
export GEM_HOME=$HOME/ruby/gems

export PATH

