<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" >
    <title>Patterns to Isolate Code in Continuous Delivery | Derek Hammer</title>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic|Cantarell:400,700,400italic,700italic|Inconsolata:400,700' rel='stylesheet' type='text/css'>
    <link rel="alternate" type="application/rss+xml" href="http://feeds.feedburner.com/derekhammer" />
    
    <link rel="stylesheet" type="text/css" media="screen" href="/css/screen.css" />
    
    <link rel="stylesheet" type="text/css" media="screen" href="/css/pygments.css" />
    

    

    

    
    <link rel="icon" type="image/png" href="/icon.png">

    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
      </script>
      <script type="text/javascript">
        try {
          var pageTracker = _gat._getTracker("UA-15522864-1");
          pageTracker._trackPageview();
        }
catch(err) {

}
</script>
    </head>
    <body>
      <div id="wrapper">
        <div id="content">
          <section class="post">
    <div class='spacer'>
      <div class='yellow'></div><div class='orange'></div><div class='red'></div><div class='purple'></div><div class='black'></div><div class='green'></div>
    </div>
    <header>
      <h1>Patterns to Isolate Code in Continuous Delivery</h1>
      <div class="date">24 Mar 2012</div>
    </header>
    <article class="post-content">
        <p>Continuous delivery is one of those goals that is very easy to understand but very difficult to implement. The gist of continuous delivery is that the development team delivers a feature as soon as it is ready. This reduces overhead and the lag time between the process of developing a feature and seeing it to the next stage. Overall, this is a good idea because it allows for features to be developed and deployed at a more rapid pace as well as enabling users to interact with the new functionality in as little time as possible.</p>

<p>This is a great and simple idea, in theory. In practice, there are some inevitable complications with that process. One of those difficulties is how to develop new features while keeping your application deployable at any given time.</p>

<p>Imagine this situation: you're doing a continuous delivery pipeline for your product. You've told business that they can deploy at any time that they wish; they have informed the team that release is to go out tomorrow night. You've just finished a feature and pushed; the pipeline went green. Now, you have 4 hours left to work on new features. However, the next story to be played is rather large and is going to take around two days to complete. Obviously, you cannot finish the feature before the next release. What is the best practice in continuous delivery going forward?</p>

<p>This is a question with a very complex answer. The one sentence answer is: you isolate your new code from the users in the simplest way possible. "The simplest way possible", though, is up to the developers of the story and they must use their judgement to implement one of the follow set of practices.</p>

<h2>Don't push until finished</h2>

<p>In this practice, you develop your feature locally until your are finished before you push any code. In a DVCS like git, this might mean that you are committing multiple times locally for a multi-day story but do not push until you are completely done. This is essentially a short-lived feature branch that is not visible to anyone else.</p>

<ul>
<li>You should use this practice for very small stories. If a story is going to take 1-2 hours to develop, go ahead and use this one. It is, by far, the easiest practice of any and it will work for smaller stories.</li>
<li>While this practice says that you should not push, it does not mean that you should not pull. Continually pull and merge your changes with what is on the master branch.</li>
<li>The downsides of this are the same of those of feature branches: the longer the local changes are not shared, the more divergent your code becomes to others. It becomes more and more a pain to refactor without significant merge conflicts and to understand the impact of any changes you make to code the longer the changes are not shared.</li>
<li>For anything of medium-to-large size, I would recommend against this practice.</li>
</ul>


<h2>Change the view last</h2>

<p>In this practice, you are modifying the underlying system to a degree that is not permeated to the client. Then, when the underlying system is ready, you use another strategy (normally #1 Don't push until finished) to do the "view" part. This can be useful for refactoring-heavy stories.</p>

<ul>
<li>For MVC web applications, a simple example is if you were adding a new endpoint that modified the controller and model as well as adding a new view. You can do the controller and model part first and then circle back to the view.</li>
<li>This can apply to non-MVC contexts, too: imagine a service that was exposing a RESTful JSON API. The JSON is the "view" in this case and if you can keep that the same you can monkey about behind it at will without effecting your consumers.</li>
<li>Personally, I like to work outside-in. I like to write ATDD tests that guide me through the entire story. Unfortunately, this practice is at odds with the working style. I would probably not employ this practice very often.</li>
</ul>


<h2>Short lived feature branches</h2>

<p>Feature branches are always a controversial topic. Please see <a href="http://derekhammer.com/2012/01/29/feature-branching-and-continuous-delivery-are-incompatible.html">feature branches and continuous delivery are incompatible</a> for more information on my take of that topic. However, for code isolation, short-lived ad-hoc feature branches can be useful. There are a slew of issues that make this an almost-last-resort type of thing for me.</p>

<ul>
<li>Short-lived ad-hoc feature branches have a life cycle the same as a story in development and are one off sorts of decisions that a developer must make.</li>
<li>The can isolate code very neatly; the code is never seen on the master branch.</li>
<li>The isolation is at cost: we are wanting to isolate features from users not code from developers. Developers need the code as soon as it is written so that they have context for a variety of circumstances. Having a pile of code being "dropped" into a code base at irregular intervals can wreak havoc on a developers ability to make changes.</li>
<li>I would only use this strategy as a last resort for a really big, breaking change that we are consciously internalizing the cost of the Big Merge.</li>
</ul>


<h2>Short-lived ad-hoc feature toggles</h2>

<p>Feature toggles are switches that exist inside of the production system which hide features from consumers. These switches can be implemented in a number of ways but all have the same effect: code is able to be continuously pushed without affected consumers.</p>

<ul>
<li>These are a bit more expensive to implement than other strategies. They also introduce technical debt and complexity into the code base.</li>
<li>Feature toggles lifecycle should be the same as the story is "in development." They should be created at the start of the story and removed when the developer thinks the story is done.</li>
<li>There is a risk that business will use the feature toggles, which is a developer tool, as a system feature. Push back on this very hard. If business wants the ability to turn features on and off, it should create requirements and go through the normal product analysis. Feature toggles can become crazily complex otherwise.</li>
<li>There are multiple ways to implement feature toggles. I have heard or seen of the following methods: using a flat text file to set values true/false that gets read on launch, using Rails environments to set boolean configuration options, using URL parameters to turn a feature on or off, using server-level redirects to prevent new features from being accessed. Each has its strengths and weaknesses. Remember: just do what is the simplest way to isolate your code.</li>
</ul>


<h2>Build a new route</h2>

<p>When developing an entirely new feature, it can sometimes be easier to build a new route (or endpoint) that is inaccessible in production. This is a relatively narrow use case: only when the new feature is entirely on its own is this useful. However, it is an important pattern to know because when it can be used, it is often the simplest method.</p>

<ul>
<li>Build the new route as you would any other feature. Leave off the linking until the end (and use strategy #1). If its a sensitive feature, consider turning off the route for production.</li>
<li>You can use any development method for this (including outside-in TDD--yay!).</li>
<li>You can also use this method if you're overhauling another view entirely. Build the new view on a new route that will be swapped in at the end.</li>
<li>Use this whenever it makes sense.</li>
</ul>


<h2>Use existing feature turn on/turn off system</h2>

<p>Some places have existing systems in place to do feature level system turn on and turn off in production. This is a product level system. It is, however, very close to what we want and surely we can reuse the system? Not from experience reports from friends on projects that are doing just this. It quickly becomes confusing what the job of the system is and whether a particular feature is a "development toggle" or a "product toggle".</p>

<ul>
<li>Do not do this.</li>
</ul>


<h2>Build a cool system like Facebook's</h2>

<p>Facebook has a system that helps them develop features and roll them out independent of the codebase. This is a really cool idea (that I unfortunately have no more insight than others about). Maybe your team should be doing this?</p>

<ul>
<li>Maybe. Consider the engineering effort to build and maintain the system. Is it worth it?</li>
</ul>


<p>What is the best practice to keep a system production-ready while still being able to continuous deliver? The answer is: it depends. You should probably be using a mixture of the seven patterns described above. There could even be other simple ways to isolate your code. The goal is to isolate your code from the users as simply as possible.</p>

    </article>
    <footer class="post-footer">
        <div class="related">
            <h3>Possibly related posts</h3>
            <ul>
                
                <li><span class="date">04-09</span><a href="/2014/04/09/on-angularjs.html">On AngularJS</a></li>
                
                <li><span class="date">04-07</span><a href="/2014/04/07/empathy-for-operations-is-a-developer-core-competency.html">Empathy for Operations is a Developer Core Competency</a></li>
                
                <li><span class="date">01-03</span><a href="/2014/01/03/freedom-of-information.html">Freedom of Information</a></li>
                
                <li><span class="date">04-23</span><a href="/2013/04/23/binding-in-swing.html">Binding in Swing</a></li>
                
                <li><span class="date">03-26</span><a href="/2013/03/26/technical-post-mortem-for-failures.html">Technical Post-Mortem for Failures</a></li>
                
                <li><span class="date">01-28</span><a href="/2013/01/28/stronger-backbone-part-3.html">Stronger Backbone - Part 3</a></li>
                
                <li><span class="date">01-27</span><a href="/2013/01/27/stronger-backbone.html">Stronger Backbone - Part 1</a></li>
                
                <li><span class="date">01-27</span><a href="/2013/01/27/strong-backbone-part-2.html">Stronger Backbone - Part 2</a></li>
                
                <li><span class="date">01-20</span><a href="/2013/01/20/a-pattern-for-rails-presenters.html">A Pattern for Rails Presenters</a></li>
                
                <li><span class="date">08-25</span><a href="/2012/08/25/maintaining-internal-gems.html">Maintaining internal gems</a></li>
                
            </ul>
        </div><div class="about">
            <h3>About Derek Hammer</h3>
            <img src="/images/hammer.png" alt="Derek Hammer" />
            <p>
                Derek Hammer writes code for a living. He loves to travel, share a good beer and try new things. He currently lives in Chicago, Illinois and works for <strong>Thought</strong>Works.
            </p>
          </div>
    </footer>
</section>

        </div>

        <div id="nav">
          <div class="nav-part">
            <h4>Site</h4>
            <ul>
              <li><a class="nav" href="/">derekhammer.com</a></li>
              
              <li><a class="nav" href="/archive.html">archive</a></li>
              
              <li><a class="nav" href="http://feeds.feedburner.com/derekhammer">rss feed</a></li>
              
              <li><a class="nav" href="/about.html">about</a></li>
              
            </ul>
          </div><div class="nav-part">
            <h4>Blog Roll</h4>
            <ul>
              
              <li><a class="nav" href="http://blogs.thoughtworks.com">ThoughtWorks</a></li>
              
              <li><a class="nav" href="http://www.22ideastreet.com">Anthony Panazzo</a></li>
              
              <li><a class="nav" href="http://jonfuller.codingtomusic.com">Jon Fuller</a></li>
              
            </ul>
          </div><div class="nav-part">
            <h4>Social Media</h4>
            <ul>
              
              <li><a class="nav" href="http://www.twitter.com/hammerdr">Twitter</a></li>
              
              <li><a class="nav" href="http://news.ycombinator.com/user?id=hammerdr">HackerNews</a></li>
              
              <li><a class="nav" href="http://www.github.com/hammerdr">GitHub</a></li>
              
            </ul>
          </div>
        </div>
      </div>

      <!-- JavaScript -->

      

      

      <a rel="author" href="https://profiles.google.com/" style="display: none;">
          <img src="https://ssl.gstatic.com/images/icons/gplus-16.png" width="16" height="16">
      </a>
    </body>
  </html>
