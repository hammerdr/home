import React from 'react';
import {createBrowserHistory} from 'history';
import {Button, Layout, Typography} from 'antd';
import { Content, Header } from 'antd/es/layout/layout';
import Sider from 'antd/es/layout/Sider';
import { background2 } from './tokens';
import {HomeFilled, ToolFilled, LineChartOutlined, FileTextFilled} from '@ant-design/icons';

const {Text, Title} = Typography;
let history = createBrowserHistory();

function LinkTree() {
  const handleHomeAssistant = React.useCallback(() => {
    history.push(`http://${window.location.hostname}:8123`);
  }, []);

  const handleCockpit = React.useCallback(() => {
    history.push(`http://${window.location.hostname}:9090`);
  }, []);

  const handleWiki = React.useCallback(() => {
    history.push(`http://doax.${window.location.hostname}`);
  }, []);

  return (
    <div style={{display: 'flex', flexDirection: 'column', gap: 8, padding: 8}}>
      <Button style={{gap: 8}} type="primary" onClick={handleWiki}>
        <FileTextFilled />
        <Text>Doax</Text>
      </Button>
      <Button style={{gap: 8}} type="primary" onClick={handleHomeAssistant}>
        <ToolFilled />
        <Text>Home Assistant (Local Only)</Text>
      </Button>
      <Button style={{gap: 8}} type="primary" onClick={handleCockpit}>
        <LineChartOutlined />
        <Text>Cockpit (Local Only)</Text>
      </Button>
    </div>
  );
}

export default function App() {
  return (
    <Layout style={{minHeight: '100vh'}}>
      <Header style={{backgroundColor: background2, display: 'flex', gap: 8}} className="header">
        <HomeFilled />
        <Title>Hammer Server</Title>
      </Header>
      <Layout>
        <Sider style={{backgroundColor: background2}} width={200}>
          <LinkTree />
        </Sider>
        <Layout>
          <Content style={{padding: 32}}>
            <Text>Content</Text>
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
}
