import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import {ConfigProvider, theme} from 'antd';
import './index.css';
import {background1, primary1, white1} from './tokens';

const container = document.getElementById('root')!;
const root = createRoot(container);
const createTheme = () => ({
  token: {
    borderRadius: 4,
    colorBgBase: background1,
    colorPrimary: primary1,
    colorTextBase: white1,
    fontSizeHeading1: 24,
  },
  algorithm: theme.darkAlgorithm,
});

root.render(
  <React.StrictMode>
    <ConfigProvider theme={createTheme()}>
      <App />
    </ConfigProvider>
  </React.StrictMode>
);
