import {generate} from '@ant-design/colors';

export const backgroundColorGradient = generate('#222E50');
export const background1 = backgroundColorGradient[5];
export const background2 = backgroundColorGradient[7];
export const background3 = backgroundColorGradient[8];
export const background4 = backgroundColorGradient[9];

export const primaryColorGradient = generate('#439A86', {
  theme: 'dark',
  backgroundColor: background1,
});
export const primary1 = primaryColorGradient[5];
export const primary2 = primaryColorGradient[7];

export const whiteColorGradient = generate('#FAF9F6', {
  theme: 'dark',
  backgroundColor: background1,
});
export const white1 = whiteColorGradient[5];
export const white2 = whiteColorGradient[9];
